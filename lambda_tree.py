import pydot
import csv
gorder = 0
class Person:
    def __init__(self, number, first, last, pledge_class):
        global gorder
        self.number = number
        self.first = first
        self.last = last
        self.pledge_class = pledge_class
        self.order = gorder
        gorder += 1

    def __repr__(self):
        return '#' + self.number + ' ' + self.first + ' ' + self.last


# Global variables
graph = pydot.Dot(graph_type='graph',
    splines='line',
    arrowhead='none',
    headport='s',
    tailport='n')

node_font = "Helvetica"

graph.set_edge_defaults(headport='n', tailport='s')

rows = []
edges = []
classes = {}
class_colors = {}
cur_class_array = []
all_members = {}
with open('dukelambdasroster_spr15.txt') as f:
    ## Read in and initialize People objects
    reader = csv.reader(f, delimiter=' ')
    cur_class = ""
    for row in reader:
        if len(row) < 1 or row[0] == '#':
            continue
        if row[0] == '<<<<':
            # start parsing in edges
            break
        if len(row) == 2:
            # Class color definition
            if row[0] in class_colors.keys():
                print row[0], " is already defined!"
                exit()
            cur_class = row[0]
            class_colors[cur_class] = row[1] 
            continue

        num = row[0]
        first = row[1]
        last = row[2]
        whole_name = first + ' ' + last
        # cur_class = row[3]
        if cur_class not in classes:
            cur_class_array = []
            cur_person = Person(num, first, last, cur_class)
            all_members[cur_person.number] = cur_person
            cur_class_array.append(cur_person)
            classes[cur_class] = cur_class_array
        else:
            cur_class_array = classes[cur_class]
            cur_person = Person(num, first, last, cur_class)
            all_members[cur_person.number] = cur_person
            cur_class_array.append(cur_person)
            classes[cur_class] = cur_class_array
        rows.append(row)
    

    # Check that there are enough class colors for every class 
    if len(class_colors.keys()) != len(classes.keys()):
        print "Error: Classes / class keys are not the same size"
        print "Colors: ", class_colors.keys(), len(class_colors.keys())
        print "Classes: ", classes.keys(), len(classes.keys())
        exit()

    ## Create Nodes
    nodes = {}
    fake_nodes = {}
    for key in all_members:
        cur_member = all_members[key]
        class_color = class_colors[cur_member.pledge_class]
        cur_node = pydot.Node(str(cur_member),
            style='filled',
            shape='rect',
            fontname=node_font,
            fillcolor=class_color,
            dpi='300')
        nodes[key] = cur_node
        fake_node = pydot.Node(str(all_members[key]) + "_fake", shape='point')
        fake_nodes[key] = fake_node
        graph.add_node(cur_node)


    ## Read in and initialize edges from file
    for edge in reader:
        if len(edge) < 1 or edge[0].startswith('#'):
            continue
        big = all_members[edge[0]]
        little = all_members[edge[1]]
        edges.append((big, little))
parent_nodes = {}
for edge in edges:
    parent_nodes[edge[0].number] = 0

for edge in edges:
    from_node = nodes[edge[0].number]
    fake_node = fake_nodes[edge[0].number]
    to_node = nodes[edge[1].number]
    parent_nodes[edge[0].number] += 1
    if parent_nodes[edge[0].number] == 1:
        ## No children yet, add node --> fake and fake --> child edges
        graph.add_node(fake_node)
        graph.add_edge(pydot.Edge(from_node, fake_node))
    # Add fake --> new edge
    graph.add_edge(pydot.Edge(fake_node, to_node))

# butadd_edge(pydot.Edge(node_d, node_a, label="and back we go again", labelfontcolor="#009933", fontsize="10.0", color="blue"))

# and we are done
graph.write_png('duke_lphie_familytree.png')
